package cmd

import (
	"encoding/hex"
	"github.com/spf13/cobra"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"os"
	//"io"
	//"github.com/cheggaaa/pb"
	//"strings"
	//"strconv"
	"time"
)

// Local options
//var user1File string
//var user2File string

func nextPartitionEsp() (next string, err error) {
	timeout := options.GetDuration("timeout")
	hostEsp := options.GetString("esp")

	client := http.Client{
		Timeout: timeout,
	}

	resp, err := client.Get("http://" + hostEsp + "/flash/next")
	if err != nil {
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	resp.Body.Close()
	next = string(body)
	return
}

func flashEsp(cmd *cobra.Command, args []string) {
	options = parseOptions(cmd)
	hostEsp := options.GetString("esp")
	user1File := options.GetString("user1")
	user2File := options.GetString("user2")
	retries := options.GetInt("retries")

	espInfo, err := getESPinfo(hostEsp)
	if err != nil {
		msgFatalf("%v\n", err)
	}
	msgInfof("Current active version: %v (%v)\n", espInfo.Version, espInfo.Partition)

	next, err := nextPartitionEsp()
	if err != nil {
		msgFatalf("%v\n", err)
	}

	var fwFile string
	switch next {
	case "user1.bin":
		fwFile = user1File
	case "user2.bin":
		fwFile = user2File
	default:
		msgFatalf("Unexpected response:\n%v", hex.Dump([]byte(next)))
	}
	msgVerbosef("Next espfs partition: %v\n", fwFile)
	client := http.Client{
		Timeout: time.Second * 60,
	}
	fw, err := os.Open(fwFile)
	if err != nil {
		msgFatalf("%v\n", err)
	}
	fwStat, err := fw.Stat()
	if err != nil {
		msgFatalf("%v\n", err)
	}

	req, err := http.NewRequest("POST", "http://"+hostEsp+"/flash/upload", fw)
	req.ContentLength = fwStat.Size()

	dump, err := httputil.DumpRequestOut(req, false)
	if err != nil {
		msgFatalf("%v\n", err)
	}
	msgDebugf("Request:\n%v\n", hex.Dump(dump))

	msgInfof("Uploading file %v (%v bytes)\n", fwFile, fwStat.Size())
	resp, err := client.Do(req)
	if err != nil {
		msgFatalf("%v\n", err)
	}

	dump, err = httputil.DumpResponse(resp, true)
	if err != nil {
		msgFatalf("%v\n", err)
	}
	msgDebugf("Response:\n%v\n", string(dump))

	time.Sleep(2000 * time.Millisecond)
	msgInfof("Rebooting ESP module\n")
	_, err = client.Get("http://" + hostEsp + "/flash/reboot")
	if err != nil {
		msgFatalf("%v\n", err)
	}

	time.Sleep(2000 * time.Millisecond)
	msgInfof("Waiting for ESP module to come back...\n")

	var i int = 0
BootWait:
	for {
		i++
		if i > retries {
			msgFatalf("No answer after %v retries... probably something went wrong\n", i)
		}
		//next, err := nextPartitionEsp()
		espInfoNew, err := getESPinfo(hostEsp)
		if err == nil {

			msgInfof("Version after reboot: %v (%v)\n", espInfoNew.Version, espInfoNew.Partition)
			//			msgVerbosef("Next espfs partition is %v\n", next)
			if espInfo.Partition != espInfoNew.Partition {
				msgInfof("Flash succeeded\n")
			} else {
				msgFatalf("Flash NOT succeeded\n")
			}
			break BootWait
		}
		time.Sleep(1000 * time.Millisecond)
	}

}

// flashCmd represents the flash command
var flashEspCmd = &cobra.Command{
	Use:   "flash <filename>",
	Short: "Flash the ESP with a new esp-link firmware.",
	Long: `Flash the ESP with a new esp-link firmware.

This command will upload a new firmware to your esp-link. See esp-link for more information`,
	Example: "  avr-link esp flash -esphost 192.168.0.78 -user1 user1.bin -user2 user2.bin",
	Run:     flashEsp,
}

func init() {
	espCmd.AddCommand(flashEspCmd)
	optEspUser1(flashEspCmd.Flags())
	optEspUser2(flashEspCmd.Flags())
	optEspHost(flashEspCmd.Flags())
	optEspTimeout(flashEspCmd.Flags())
	optEspRetries(flashEspCmd.Flags())

}
