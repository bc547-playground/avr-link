package cmd

import (
	"github.com/spf13/cobra"
	"io"
	"net"
	"os"
)

func serial(cmd *cobra.Command, args []string) {
	options = parseOptions(cmd)
	hostEsp := options.GetString("esp")

	conn, err := net.Dial("tcp", hostEsp+":23")
	if err != nil {
		msgFatalf("%v\n", err)
	}
	msgVerbosef("Connected...\n")

	stdin, stdout := make(chan bool), make(chan bool)
	cerr := make(chan error)

	go func() {
		_, err := io.Copy(conn, os.Stdin)
		if err != nil {
			cerr <- err
		}
		close(stdin)
	}()
	go func() {
		_, err := io.Copy(os.Stdout, conn)
		if err != nil {
			cerr <- err
		}
		close(stdout)
	}()

	// Wait for either side to be closed, then exit
	select {
	case <-stdin:
	case <-stdout:
	case err := <-cerr:
		msgFatalf("%v\n", err)
	}

}

// serialCmd represents the serial command
var serialCmd = &cobra.Command{
	Use:   "serial",
	Short: "Open a serial connection.",
	Long:  `This command opens a serial terminal to your arduino.`,
	Run:   serial,
}

func init() {
	RootCmd.AddCommand(serialCmd)
	optEspHost(serialCmd.Flags())
}
