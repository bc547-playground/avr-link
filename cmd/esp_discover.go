package cmd

import (
	"github.com/spf13/cobra"
	"net"
)

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func tryip(result chan *espSystemInfo, ip string) {
	//msgDebugf("Trying ip: %+v\n", ip)

	espInfo, err := getESPinfo(ip)
	if err != nil {
		result <- nil
		return
	}
	if espInfo.Version != "" {
		result <- espInfo
	} else {
		result <- nil
	}
}

func discover(cmd *cobra.Command, args []string) {
	options = parseOptions(cmd)
	networkStr := options.GetString("network")
	_, discoverIPNet, err := net.ParseCIDR(networkStr)
	if err != nil {
		msgFatalf("network option - %v\n", err)
	}

	msgDebugf("Network to scan: %+v\n", discoverIPNet)

	results := make(chan *espSystemInfo)

	i := 0
	ip := discoverIPNet.IP
	ipnet := discoverIPNet
	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); inc(ip) {
		go tryip(results, ip.String())
		i++
	}

	msgVerbosef("Scanning %v hosts...\n", i)
	for i > 0 {
		result := <-results
		if result != nil {
			msgInfof("ip:%v name:%v description:%v\n", result.IP, result.Name, result.Description)
		}
		i--
	}
}

// discoverCmd represents the discover command
var discoverCmd = &cobra.Command{
	Use:   "discover",
	Short: "Scan for esp-link modules on your network.",
	Long:  `This command scans for active ESP8266 module with esp-link firmware on your network.`,
	Run:   discover,
}

func init() {
	espCmd.AddCommand(discoverCmd)
	//_, defIPNet, _ := net.ParseCIDR("192.168.0.0/24")
	//discoverCmd.Flags().IPNetVarP(&discoverIPNet, "network", "n", *defIPNet, "network to scan")
	optNetwork(discoverCmd.Flags())
}
