# What is avr-link
This tool is used to manage an ESP8266 module with esp-link firmware ( https://github.com/jeelabs/esp-link ) and also an attached arduino compatible MCU. All command line options can also be specified using a configuration file. This makes day to day use much easier since all options will be properly filled in automatically.

**NOTE: At this moment, the MCU can only be remotely flashed with a new sketch if the device uses the stk500v1 programming protocol.**

Since this code is written in golang and uses no OS specific packages, it should compile and run on all platforms 
(FreeBSD 8-STABLE or later, Linux 2.6.23 or later with glibc (also arm based), Mac OS X 10.7 or later, Windows XP or later)

Originally this tool started as an implementation of the avrflash tool that's part of the esp-link ESP8266 firmware. It allows OTA flashing of arduino style MCU's. But soon after that, an implementation of the wiflash tool was added. Check out the esp-link pages for all details about these tools. But there are much more features that would come in handy when developing wireless arduino projects.

The goal is to make a platform independent tool to completely manage both esp-link and an arduino attached board (flash, serial, discover, reset...)

# Releases and binaries

See https://gitlab.com/bc547-playground/avr-link/tags

# Usage

To get the help, just run avr-link with the -h or --help flag.

All (or just some if you want) options can also be specified with a configuration file. For more information about this, see the *avr-link.toml_example* file.

### OTA flashing of arduino sketch

Suppose your ESP module has an ip adress 192.168.0.76 and you want to upload the file arduino.hex to the arduino attached to the ESP module 
with esp-link. In that case, you can upload the file with ```avr-link flash --esp 192.168.0.76 --file arduino.hex```

You can also put these parameters in a configfile called avr-link.toml:

```
[flash]
esp="192.168.0.76"
file="arduino.hex"
```

If you want to upload these file, it now suffices to just run ```avr-link flash```


### OTA flashing of ESP8266 module with esp-link firmware

Suppose your ESP module has an ip adress 192.168.0.76 and you want to upload a new esp-link firmware to it.
In that case, you can upload the file with ```avr-link esp flash --esp 192.168.0.76 --user1 user1.bin --user2 user2.bin```

You can also put these parameters in a configfile called avr-link.toml:

```
[esp.flash]
esp="192.168.0.76"
user1="user1.bin"
user2="user2.bin"
```

If you want to upload these file, it now suffices to just run ```avr-link esp flash```

# Compiling yourself

- Make sure Golang is installed
- Make a directory where you want to download the source and compile
- Enter that directory
- Run ``export GOPATH=$PWD``
- Run ``go get -v gitlab.com/bc547-playground/avr-link``
- The compiled binary can be found in the ./bin directoy
