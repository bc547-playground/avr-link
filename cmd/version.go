package cmd

import (
	"github.com/spf13/cobra"
)

var Version = "master"
var BuildStamp = "0"
var Githash = "HEAD"
var Target = "unknown"

func version(cmd *cobra.Command, args []string) {
	msgInfof("Version: %v\n", Version)
	msgInfof("Build Time: %v\n", BuildStamp)
	msgInfof("Target Platform: %v\n", Target)
	msgInfof("Git Commit Hash: %v\n", Githash)
}

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version.",
	Long:  `Print the version.`,
	Run:   version,
}

func init() {
	RootCmd.AddCommand(versionCmd)
}
