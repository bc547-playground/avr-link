package cmd

import (
	"encoding/hex"
	"github.com/spf13/cobra"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func flash(cmd *cobra.Command, args []string) {
	options = parseOptions(cmd)
	timeout := options.GetDuration("timeout")
	hexFile := options.GetString("file")
	hostEsp := options.GetString("esp")
	retries := options.GetInt("retries")

	//
	// Some housekeeping
	//
	client := http.Client{
		Timeout: timeout,
	}
	// Try to read the sketch first before resetting the AVR
	if hexFile == "" {
		msgFatalUsagef("Missing sketch filename\n")
	}
	hexdata, err := ioutil.ReadFile(hexFile)
	if err != nil {
		msgFatalf("%v\n", err)
	}

	//
	// Initiate AVR reset
	//
	msgVerbosef("Initiating AVR reset\n")
	resp, err := client.Post("http://"+hostEsp+"/pgm/sync", "text/plain", nil)
	if err != nil {
		msgFatalf("%v\n", err)
	}
	if resp.StatusCode != 204 {
		msgFatalf("Expect status code 204, but got %v! (Full response: %+v)\n", resp.StatusCode, resp)
	}
	msgInfof("AVR reset initiated...waiting for SYNC status\n")

	//
	// Wait for sync
	//
	var i int = 0
SyncWait:
	for {
		i++
		if i > retries {
			msgFatalf("No sync after %v retries... giving up\n", i)
		}
		resp, err := client.Get("http://" + hostEsp + "/pgm/sync")
		if err != nil {
			msgFatalf("%v\n", err)
		}
		body, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		state := string(body)
		switch {
		case strings.HasPrefix(state, "SYNC"):
			msgInfof("AVR in sync\n")
			break SyncWait
		case strings.HasPrefix(state, "NOT READY"):
			msgInfof(".\n")
		default:
			msgFatalf("Unexpected response:\n%v", hex.Dump(body))
		}
		time.Sleep(100 * time.Millisecond)
	}

	//
	// Write the HEX file
	//
	resp, err = client.Post("http://"+hostEsp+"/pgm/upload", "application/x-www-form-urlencoded", strings.NewReader(string(hexdata)))
	if err != nil {
		msgFatalf("%v\n", err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	msgInfof("result: %+v\n", string(body))

}

// flashCmd represents the flash command
var flashCmd = &cobra.Command{
	Use:   "flash <filename>",
	Short: "Flash the MCU with a new sketch.",
	Long: `Flash the MCU with a new sketch.

This command will upload a new sketch file to your arduino. The sketch to be uploaded must be in .hex format`,
	Example: "  avr-link flash --esp 192.168.0.78 --file arduino.hex",
	Run:     flash,
}

func init() {
	RootCmd.AddCommand(flashCmd)
	optSketchFile(flashCmd.Flags())
	optEspHost(flashCmd.Flags())
	optEspTimeout(flashCmd.Flags())
	optEspRetries(flashCmd.Flags())
	//	flashCmd.Flags().StringP("esp", "e", "esp-link.local", "hostname or ip of the ESP module with esp-link")
	//	flashCmd.Flags().DurationP("timeout", "t", time.Second*10, "timeout for each http request")
	//	flashCmd.Flags().IntP("retries", "r", 20, "number of http requests to verify sync mode before we give up")
}
