package cmd

import (
	"github.com/spf13/cobra"
)

// espCmd represents the esp command
var espCmd = &cobra.Command{
	Use:   "esp",
	Short: "ESP commands.",
	Long:  `This command allows management of the ESP module itself (instead of the attached arduino MCU).`,
}

func init() {
	RootCmd.AddCommand(espCmd)
}
