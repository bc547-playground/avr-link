package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"net/http"
	"os"
	"reflect"
	"sort"
	"strings"
	"time"
)

var options *viper.Viper

func optSketchFile(flagset *pflag.FlagSet) {
	flagset.StringP("file", "f", "", "name of the sketch .hex file to upload")
}
func optEspTimeout(flagset *pflag.FlagSet) {
	flagset.DurationP("timeout", "t", time.Second*10, "maximum time to wait for a single http request to the ESP module")
}
func optEspRetries(flagset *pflag.FlagSet) {
	flagset.IntP("retries", "r", 20, "how many times to retry a failed http request to the ESP module")
}
func optEspHost(flagset *pflag.FlagSet) {
	flagset.StringP("esp", "e", "esp-link.local", "hostname or ip address of the ESP module with esp-link")
}
func optEspUser1(flagset *pflag.FlagSet) {
	flagset.StringP("user1", "1", "user1.bin", "esp-link user1.bin firmware file")
}
func optEspUser2(flagset *pflag.FlagSet) {
	flagset.StringP("user2", "2", "user2.bin", "esp-link user2.bin firmware file")
}
func optNetwork(flagset *pflag.FlagSet) {
	flagset.StringP("network", "n", "", "network range to scan for esp-link nodes (e.g. 192.168.0.0/24)")
}

// Log a Fatal message and add some --help reference
func msgFatalf(format string, v ...interface{}) {
	os.Stderr.Write([]byte(fmt.Sprintf("FATAL ERROR: "+format, v...)))
	os.Exit(-1)
}

func msgFatalUsagef(format string, v ...interface{}) {
	msgFatalf(fmt.Sprintf(format+
		"\nUse --help to get more information about usage\n", v...))
}

func msgInfof(format string, v ...interface{}) {
	os.Stdout.Write([]byte(fmt.Sprintf(format, v...)))
}

func msgVerbosef(format string, v ...interface{}) {
	if options.GetBool("verbose") {
		os.Stdout.Write([]byte(fmt.Sprintf(format, v...)))
	}
}

func msgDebugf(format string, v ...interface{}) {
	if options.GetBool("debug") {
		os.Stdout.Write([]byte(fmt.Sprintf("debug: "+format, v...)))
	}
}

func msgDump(data interface{}) {
	spew.Dump(data)
}

func parseOptions(cmd *cobra.Command) (options *viper.Viper) {
	cmdChain := strings.Split(cmd.CommandPath(), " ")
	key := strings.Join(cmdChain[1:], ".") // remove the avr-link word (1st element)

	if viper.IsSet(key) {
		options = viper.Sub(key)
	} else {
		options = viper.New()
	}
	options.BindPFlags(cmd.Flags())
	if options.GetBool("show-config") {
		msgInfof("Config file used: %v\n\n", viper.ConfigFileUsed())
		msgInfof("[%v]\n", key)
		keys := options.AllKeys()
		sort.Strings(keys)
		for _, key := range keys {
			if key == "help" || key == "show-config" || key == "config" {
				continue
			}
			data := options.Get(key)
			formatter := "%v=%v\n"
			if reflect.TypeOf(data).Kind().String() == "string" {
				formatter = "%v=\"%v\"\n"
			}
			msgInfof(formatter, key, data)
		}
		//spew.Dump(options.AllSettings())
		os.Exit(0)
	}
	return
}

type espSystemInfo struct {
	Baud        string   `json:"baud"`
	Description string   `json:"description"`
	ID          string   `json:"id"`
	Mqtt        string   `json:"mqtt"`
	Name        string   `json:"name"`
	Partition   string   `json:"partition"`
	Reset_cause string   `json:"reset cause"`
	Size        string   `json:"size"`
	Slip        string   `json:"slip"`
	Chan        int      `json:"chan"`
	Dhcp        string   `json:"dhcp"`
	Gateway     string   `json:"gateway"`
	Hostname    string   `json:"hostname"`
	IP          string   `json:"ip"`
	Mac         string   `json:"mac"`
	Mode        string   `json:"mode"`
	Modechange  string   `json:"modechange"`
	Netmask     string   `json:"netmask"`
	Phy         string   `json:"phy"`
	Rssi        string   `json:"rssi"`
	Ssid        string   `json:"ssid"`
	Staticip    string   `json:"staticip"`
	Status      string   `json:"status"`
	Warn        string   `json:"warn"`
	Conn        int      `json:"conn"`
	Isp         int      `json:"isp"`
	Reset       int      `json:"reset"`
	Rxpup       int      `json:"rxpup"`
	Ser         int      `json:"ser"`
	Swap        int      `json:"swap"`
	Menu        []string `json:"menu"`
	Version     string   `json:"version"`
}

// http://stackoverflow.com/questions/17156371/how-to-get-json-response-in-golang
func getJson(url string, target interface{}) error {
	timeout, _ := time.ParseDuration("5s")
	client := http.Client{
		Timeout: timeout,
	}
	r, err := client.Get(url)
	if err != nil {
		return err
	}
	if r.StatusCode != 200 {
		return errors.New("Invalid status code")
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func getESPinfo(ip string) (info *espSystemInfo, err error) {
	msgDebugf("getESPinfo ip: %+v\n", ip)

	info = new(espSystemInfo)
	err = getJson("http://"+ip+"/system/info", info)
	if err != nil {
		return
	}
	getJson("http://"+ip+"/wifi/info", info)
	getJson("http://"+ip+"/pins", info)
	getJson("http://"+ip+"/menu", info)

	//  msgInfof("%v - connected - %+v\n", ip, info)
	//  bytes, _ := json.MarshalIndent(info, "", "  ")
	//  msgInfof(string(bytes))
	return
}
