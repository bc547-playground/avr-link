# This configfile uses the toml syntax. See https://github.com/toml-lang/toml for more information
#
# You can override for each avr-link command the default value that will be used. 
# The precedence order is:
# - if a cmdline option is found, the cmdline value will be used
# - if an option is not specified on cmdline, the corresponding value in the config file
#   will be used
# - if an option is not found in the config file, the built-in default value is used.
#   this default can be seen with the --help option
#
# Example:
# For the 'serial' command, it is mandatory to specify the hostname or ip address of the esp module
# to connect to. Let's assume the ip address is 192.168.0.76. This would make the command:
#
# avrlink serial --esp 192.168.0.76
#
# Let's create a config file called avr-link.toml with the following content:
# --- avr-link.toml ---
# [serial]
# esp="192.168.0.76"
# ---------------------
# 
# This file will autofill the 'esp' option with 192.168.0.76. So if you want to open
# a serial connection, it suffices to run
#
# avr-link serial
#
# If you have several projects, each project can have its own config file with the correct values
# for that project and you don't have to remember anything. 
#
# Any available option for a command can be specified. See 'avr-link <command> --help' for an 
# overview of the available options
#
# Note1: for subcommands (e.g. esp discover), the header would be [esp.discover]
#
# Note2: variable interpolation is not possible at this moment. Neither is there is no support 
#        yet for hierachical options. This means if several command require the same option, 
#        you will need to repeat the complete option setting several times (once for each command)
#
[serial]
esp="192.168.0.76"

[esp.flash]
esp="192.168.0.76"

[esp.discover]
network="192.168.0.0/24"

[flash]
timeout="5s"
debug=true
verbose=true
file="arduino.hex"
esp="192.168.0.76"
