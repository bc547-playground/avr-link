package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
)

var configFile string

// This represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "avr-link",
	Short: "This tool is used to manage an ESP8266 module with esp-link firmware and attached arduino compatible MCU.",
	Long: `This tool is used to manage an ESP8266 module with esp-link firmware ( https://github.com/jeelabs/esp-link ) and also an attached arduino compatible MCU. All command line options can also be specified using a configuration file. This makes day to day use much easier since all options will be properly filled in automatically.

*note* At this moment, the MCU can only be remotely flashed with a new sketch if the device uses the stk500v1 programming protocol.
`,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	RootCmd.PersistentFlags().StringVarP(&configFile, "config", "c", "", "config file (allowed extensions: json,toml,yaml,yml,hcl)")
	RootCmd.PersistentFlags().Bool("show-config", false, "Show the 'real used' values of all options for a command and exit.")
	RootCmd.PersistentFlags().BoolP("verbose", "v", false, "show more verbose output")
	RootCmd.PersistentFlags().BoolP("debug", "d", false, "show debug output")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {

	// Search for a config file if none is given
	ConfigPath := []string{".", "$HOME", "/etc", "/etc/avr-link"}

	if configFile == "" {
	configSearch:
		for _, path := range ConfigPath {
			for _, ext := range viper.SupportedExts {
				tryfile := os.ExpandEnv(filepath.Join(path, "avr-link."+ext))
				if _, err := os.Stat(tryfile); !os.IsNotExist(err) {
					// path/to/whatever does exist
					configFile = tryfile
					break configSearch
				}
			}
		}
	}

	if configFile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(configFile)
		if err := viper.ReadInConfig(); err == nil {
			//msgVerbosef("Using config file: %v\n", viper.ConfigFileUsed())
		} else {
			msgFatalf("Configfile error - %v\n", err)
		}
	}
}
