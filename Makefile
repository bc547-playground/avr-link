VERSION=v0.5

DATE=$(shell date --iso-8601=seconds)
GITHASH=$(shell git rev-parse HEAD)
PACKAGE=gitlab.com/bc547-playground/avr-link/cmd
LDFLAGS=-X $(PACKAGE).BuildStamp=$(DATE) -X $(PACKAGE).Githash=$(GITHASH) -X $(PACKAGE).Version=$(VERSION)

all: win32 win64 linux32 linux64

win32:
	-GOOS=windows GOARCH=386 go get -v gitlab.com/bc547-playground/avr-link
	GOOS=windows GOARCH=386 go build -o bin/windows/386/avr-link.exe -v -ldflags '$(LDFLAGS) -X $(PACKAGE).Target=windows-386' gitlab.com/bc547-playground/avr-link 
	zip -9 -v -j avr-link_$(VERSION)_win32.zip bin/windows/386/avr-link.exe

win64:
	-GOOS=windows GOARCH=amd64 go get -v gitlab.com/bc547-playground/avr-link
	GOOS=windows GOARCH=amd64 go build -o bin/windows/x64/avr-link.exe -v -ldflags '$(LDFLAGS) -X $(PACKAGE).Target=windows-amd64' gitlab.com/bc547-playground/avr-link
	zip -9 -v -j avr-link_$(VERSION)_win64.zip bin/windows/x64/avr-link.exe

linux64:
	-GOOS=linux GOARCH=amd64 go get -v gitlab.com/bc547-playground/avr-link
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o bin/linux/x64/avr-link -a -installsuffix cgo -ldflags '-s $(LDFLAGS) -X $(PACKAGE).Target=linux-amd64' -v gitlab.com/bc547-playground/avr-link
	tar -czv -C bin/linux/x64 -f avr-link_$(VERSION)_linux64.tgz avr-link

linux32:
	-GOOS=linux GOARCH=386 go get -v gitlab.com/bc547-playground/avr-link
	GOOS=linux GOARCH=386 CGO_ENABLED=0 go build -o bin/linux/386/avr-link -a -installsuffix cgo -ldflags '-s $(LDFLAGS) -X $(PACKAGE).Target=linux-386' -v gitlab.com/bc547-playground/avr-link
	tar -czv -C bin/linux/386 -f avr-link_$(VERSION)_linux32.tgz avr-link

clean:
	-rm -rf bin
	-rm -rf *.zip
	-rm -rf *.tgz
	-rm -rf avr-link
